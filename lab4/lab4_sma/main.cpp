#include <QCoreApplication>
#include "sma.h"

int main(int argc, char *argv[])
{
    Sma sma;
    sma.calculate_sma();

    QApplication a(argc, argv);

    QLineSeries *series = new QLineSeries();
    QLineSeries *series_sma = new QLineSeries();

    series->setName("Начальный график");
    for (unsigned int i = 0; i < sma.result.size(); i++)
    {
        series->append(sma.os_x[i], sma.os_y[i]);
    }

    series_sma ->setName("График SMA");
    for (unsigned int i = 0; i < sma.result.size(); i++)
    {
        series_sma->append(sma.os_x[i], sma.result[i]);
    }

    QChart *chart = new QChart();
    chart->legend()->setVisible(true);
    chart->addSeries(series);
    chart->addSeries(series_sma);
    chart->createDefaultAxes();
    chart->setTitle("Самодельная SMA");

    QChartView *chartView = new QChartView(chart);
    chartView->setRenderHint(QPainter::Antialiasing);

    QMainWindow window;
    window.setCentralWidget(chartView);
    window.resize(900, 700);
    window.show();

    return a.exec();
}
