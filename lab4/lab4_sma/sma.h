#ifndef SMA_H
#define SMA_H
#include <vector>
#include <iostream>
#include <QTextCodec>
#include <ctime>
#include <QtWidgets/QApplication>
#include <QtWidgets/QMainWindow>
#include <QtCharts/QChartView>
#include <QtCharts/QLineSeries>
using namespace std;
QT_CHARTS_USE_NAMESPACE


class Sma
{
public:
    Sma();

    //Определяем итератор для среза
    typedef vector<float>::iterator vec_iter;

    float window;
    vector<float> os_x = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30,
                         31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50};
    vector<float> os_y;
    vector<float> sma_y;
    vector<float> result;

    //Заполнения вектора рандомными числами от 0 до 100
    void rand_vector();

    //Срез вектора, может есть такая функция но я не нашел
    vector<float> slice_vector(vec_iter first, vec_iter last);

    //Расчет SMA
    void calculate_sma();
};

#endif // SMA_H
