#include "sma.h"

Sma::Sma()
{
}

//Заполнения вектора рандомными числами от 0 до 100
void Sma::rand_vector()
{
    srand(time(NULL));
    for (int i = 0; i < 50; i++)
    {
        os_y.push_back(rand() % 101);
    }
}

//Срез вектора, может есть такая функция но я не нашел
vector<float> Sma::slice_vector(vec_iter first, vec_iter last)
{
    sma_y.clear();
    for (vec_iter cur = first; cur != last; ++cur)
    {
        sma_y.push_back(*cur);
    }
    return sma_y;
}

//Расчет SMA
void Sma::calculate_sma()
{
    // Костыль для отображения кириллицы в консоли
    QTextCodec *codec = QTextCodec::codecForName("CP866");

    rand_vector();

    start_cin:

    // Продолжение костыля
    QByteArray encodedString = codec->fromUnicode("Введите величину окна (тип float):");
    cout << encodedString.data() << endl;

    cin >> window;

    // Проверка ввода
    if (!cin or window == 0)
    {
        cin.clear();
        cin.ignore(numeric_limits<streamsize>::max(), '\n');

        // Продолжение костыля
        encodedString = codec->fromUnicode("Неверное значение, попробуйте снова:");
        cout << encodedString.data() << endl;

        goto start_cin;
    }

    int len = os_y.size();
    int sma_count = 0;
    int window_temp = window;

    //Расчет по формуле (p_1 + p_2 + p_3 + ... + p_n) / n
    while(true)
    {
        float sum = 0;
        slice_vector(os_y.begin() + sma_count, os_y.begin() + window_temp);
        sum = accumulate(sma_y.begin(), sma_y.end(), 0.0);
        result.push_back(sum/window);
        sma_count += 1;
        window_temp += 1;
        if (window_temp > len)
        {
            break;
        }
    }

//        Проверка результата

//        for (int i = 0; i < result.size(); i++)
//        {
//            cout <<"RESULT "<< result[i] << endl;
//        }
}
