#include <QtTest>
#include "../lab4_sma/sma.h"

class Test_sma : public QObject
{
    Q_OBJECT

public:
    Test_sma();
    ~Test_sma();

private slots:
    void test_slice_vector();
};

Test_sma::Test_sma()
{
}

Test_sma::~Test_sma()
{
}

// Тест среза вектора, так как нашел функцию среза в QT, которую можно взять за эталон
void Test_sma::test_slice_vector()
{
    Sma test_sma;
    QVector<float> test = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30,
            31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50};
    vector<float> os_x = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30,
                         31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50};
    vector<float> os_x_result = test_sma.slice_vector(os_x.begin() + 1, os_x.end() - 4);

    QVector<float> Qos_x(os_x_result.begin(), os_x_result.end());

    QCOMPARE(Qos_x, test.mid(1, 45));

//    qDebug() << test_sma.slice_vector(os_x.begin() + 1, os_x.end() - 4);
//    qDebug() << test.mid(1, 45);
}

QTEST_APPLESS_MAIN(Test_sma)

#include "tst_test_sma.moc"
