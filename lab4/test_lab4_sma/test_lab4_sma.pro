QT += testlib
QT -= gui

CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

TEMPLATE = app

SOURCES +=  tst_test_sma.cpp \
    ../lab4_sma/sma.cpp

HEADERS += \
    ../lab4_sma/sma.h
