#include <QtTest>
#include "../lab1_calculator/calculator.h"

class Test_lab1 : public QObject
{
    Q_OBJECT

public:
    Test_lab1();
    ~Test_lab1();

private slots:
    void test_sum();
    void test_sum_data();
    void test_sub();
    void test_sub_data();
    void test_mult();
    void test_mult_data();
    void test_div();
    void test_div_data();
};

Test_lab1::Test_lab1()
{
}

Test_lab1::~Test_lab1()
{
}

// Заполняем данные для тестов рандомными числами
void Test_lab1::test_sum_data()
{
    QTest::addColumn<double>("a");
    QTest::addColumn<double>("b");
    QTest::addColumn<double>("c");

    for(int x = 0; x < 10; ++x)
    {
        double a = rand() % 1000;
        double b = rand() % 1000;

        QTest::newRow("sum") << a << b << a + b;
//        qDebug() << a;
//        qDebug() << b;
    }
}

void Test_lab1::test_sub_data()
{
    QTest::addColumn<double>("a");
    QTest::addColumn<double>("b");
    QTest::addColumn<double>("c");

    for(int x = 0; x < 10; ++x)
    {
        double a = rand() % 1000;
        double b = rand() % 1000;

        QTest::newRow("sub") << a << b << a - b;
//        qDebug() << a;
//        qDebug() << b;
    }
}

void Test_lab1::test_mult_data()
{
    QTest::addColumn<double>("a");
    QTest::addColumn<double>("b");
    QTest::addColumn<double>("c");

    for(int x = 0; x < 10; ++x)
    {
        double a = rand() % 1000;
        double b = rand() % 1000;

        QTest::newRow("mult") << a << b << a * b;
//        qDebug() << a;
//        qDebug() << b;
    }
}

void Test_lab1::test_div_data()
{
    QTest::addColumn<double>("a");
    QTest::addColumn<double>("b");
    QTest::addColumn<double>("c");

    for(int x = 0; x < 10; ++x)
    {
        double a = rand() % 1000;
        double b = rand() % 1000;

        QTest::newRow("div") << a << b << std::round(a / b*100000)/100000;
//        qDebug() << a;
//        qDebug() << b;
    }
}

void Test_lab1::test_sum()
{
    Calculator test_calc;
    QFETCH(double, a);
    QFETCH(double, b);
    QFETCH(double, c);
    QCOMPARE(test_calc.sum(a, b), c);
}

void Test_lab1::test_sub()
{
    Calculator test_calc;
    QFETCH(double, a);
    QFETCH(double, b);
    QFETCH(double, c);
    QCOMPARE(test_calc.sub(a, b), c);
}

void Test_lab1::test_mult()
{
    Calculator test_calc;
    QFETCH(double, a);
    QFETCH(double, b);
    QFETCH(double, c);
    QCOMPARE(test_calc.mult(a, b), c);
}

void Test_lab1::test_div()
{
    Calculator test_calc;
    QFETCH(double, a);
    QFETCH(double, b);
    QFETCH(double, c);
    QCOMPARE(std::round(test_calc.div(a, b)*100000)/100000, c);
}

QTEST_APPLESS_MAIN(Test_lab1)

#include "tst_test_lab1.moc"
