#include <QCoreApplication>
#include "calculator.h"

int main(int argc, char *argv[])
{
    // Костыль для отображения кириллицы в консоли
    QTextCodec *codec = QTextCodec::codecForName("CP866");

    int choose;
    Calculator calc;

    start_label:
    // Продолжение костыля
    QByteArray encodedString = codec->fromUnicode("Введите первый операнд:");
    cout << encodedString.data() << endl;
    cin >> calc.a;
    //Проверка вводимых данных
    if (!cin)
        {
            calc.clean_input();
            goto start_label;
        }
    encodedString = codec->fromUnicode("Введите второй операнд:");
    cout << encodedString.data() << endl;
    cin >> calc.b;
    if (!cin)
        {
            calc.clean_input();
            goto start_label;
        }

    choose_label:
    encodedString = codec->fromUnicode("Выберите действие:");
    cout << encodedString.data() << endl;
    encodedString = codec->fromUnicode("1 - сложение, 2 - вычитание, 3 - умножение, 4 - деление:");
    cout << encodedString.data() << endl;
    cin >> choose;
    if (!cin)
        {
            calc.clean_input();
            goto choose_label;
        }
    if (choose == 1)
    {
        calc.sum(calc.a, calc.b);
        calc.a = calc.c;
        QTextCodec *codec = QTextCodec::codecForName("CP866");
        QByteArray encodedString = codec->fromUnicode("Ответ:");
        cout << encodedString.data() << calc.c << endl;
    }
    else if (choose == 2)
    {
        calc.sub(calc.a, calc.b);
        calc.a = calc.c;
        QTextCodec *codec = QTextCodec::codecForName("CP866");
        QByteArray encodedString = codec->fromUnicode("Ответ:");
        cout << encodedString.data() << calc.c << endl;
    }
    else if (choose == 3)
    {
        calc.mult(calc.a, calc.b);
        calc.a = calc.c;
        QTextCodec *codec = QTextCodec::codecForName("CP866");
        QByteArray encodedString = codec->fromUnicode("Ответ:");
        cout << encodedString.data() << calc.c << endl;
    }
    else if (choose == 4)
    {
        if (calc.b == 0)
        {
            encodedString = codec->fromUnicode("На 0 делить нельзя!");
            cout << encodedString.data() << endl;
            goto start_label;
        }
        calc.div(calc.a, calc.b);
        calc.a = calc.c;
        QTextCodec *codec = QTextCodec::codecForName("CP866");
        QByteArray encodedString = codec->fromUnicode("Ответ:");
        cout << encodedString.data() << calc.c << endl;
    }
    else
    {
        encodedString = codec->fromUnicode("Нет такого действия, повторите выбор");
        cout << encodedString.data() << endl;
        cin.clear();
        cin.ignore(numeric_limits<streamsize>::max(), '\n');
        goto choose_label;
    }

    choose_label_2:
    encodedString = codec->fromUnicode("Выберите дальнейшее действие:");
    cout << encodedString.data() << endl;
    encodedString = codec->fromUnicode("1 - продолжить работу с результатом, 2 - ввести новые операнды, 3 - завершить работу");
    cout << encodedString.data() << endl;
    cin >> choose;
    if (!cin)
    {
        calc.clean_input();
        goto choose_label_2;
    }
    if (choose == 1)
    {
        error_b:
        encodedString = codec->fromUnicode("Введите второй операнд:");
        cout << encodedString.data() << endl;
        cin >> calc.b;
        if (!cin)
        {
            calc.clean_input();
            goto error_b;
        }
        goto choose_label;
    }
    else if (choose == 2)
    {
        goto start_label;
    }
    else if (choose == 3)
    {
        return 0;
    }
    else
    {
        encodedString = codec->fromUnicode("Нет такого действия, повторите выбор");
        cout << encodedString.data() << endl;
        cin.clear();
        cin.ignore(numeric_limits<streamsize>::max(), '\n');
        goto choose_label_2;
    }

    QCoreApplication a(argc, argv);
}
