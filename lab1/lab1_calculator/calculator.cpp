#include "calculator.h"

Calculator::Calculator()
{
}

double Calculator::sum(double a, double b)
{
    c = a + b;
    return c;
}

double Calculator::sub(double a, double b)
{
    c = a - b;
    return c;
}

double Calculator::mult(double a, double b)
{
    c = a * b;
    return c;
}

double Calculator::div(double a, double b)
{
    c = a / b;
    return c;
}
//Очищаем поток и очередь ввода
void Calculator::clean_input()
{
    cin.clear();
    cin.ignore(numeric_limits<streamsize>::max(), '\n');
    QTextCodec *codec = QTextCodec::codecForName("CP866");
    QByteArray encodedString = codec->fromUnicode("Ошибка. Введеные данные не являются числом, повторите ввод");
    cout << encodedString.data() << endl;
}
