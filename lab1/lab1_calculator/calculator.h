#ifndef CALCULATOR_H
#define CALCULATOR_H
#include <QTextCodec>
#include <iostream>
#include <limits>
#include <cmath>
using namespace std;

class Calculator
{
public:
    Calculator();

    float a, b, c;

    double sum(double a, double b);
    double sub(double a, double b);
    double mult(double a, double b);
    double div(double a, double b);
    //Очищаем поток и очередь ввода
    void clean_input();
};

#endif // CALCULATOR_H
