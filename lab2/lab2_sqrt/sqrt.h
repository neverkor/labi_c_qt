#ifndef SQRT_H
#define SQRT_H
#include <iostream>
#include <limits>
#include <QTextCodec>
using namespace std;

class Sqrt
{
public:
    Sqrt();
    //Очищаем поток и очередь ввода
    void clean_input();
    double sqrt_result(double x, double y);
private:
    int count = 0;
};

#endif // SQRT_H
