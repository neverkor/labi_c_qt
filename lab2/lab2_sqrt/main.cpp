#include <QCoreApplication>
#include "sqrt.h"

int main(int argc, char *argv[])
{
    double x, y;
    Sqrt sqrt;

    start_x:
    QTextCodec *codec = QTextCodec::codecForName("CP866");
    QByteArray encodedString = codec->fromUnicode("Введите число, из которого нужно расчитать корень: ");
    cout << encodedString.data() << endl;
    cin >> x;
    //Проверка вводимых данных
    if (x == 0)
    {
        encodedString = codec->fromUnicode("Корень из ");
        cout << encodedString.data() << x;
        encodedString = codec->fromUnicode(" равен: 0");
        cout << encodedString.data() << endl;
    }

    if (!cin)
    {
        sqrt.clean_input();
        goto start_x;
    }

    start_y:
    encodedString = codec->fromUnicode("Введите свое предположение, чему он равен: ");
    cout << encodedString.data() << endl;
    cin >> y;
    if (!cin)
    {
        sqrt.clean_input();
        goto start_y;
    }

    sqrt.sqrt_result(x, y);

    QCoreApplication a(argc, argv);
    return 0;
}
