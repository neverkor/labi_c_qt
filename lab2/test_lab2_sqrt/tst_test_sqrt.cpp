#include <QtTest>
#include <cmath>
#include "../lab2_sqrt/sqrt.h"

class Test_sqrt : public QObject
{
    Q_OBJECT

public:
    Test_sqrt();
    ~Test_sqrt();

private slots:
    void test_sqrt_result();
    void test_sqrt_result_data();

};

Test_sqrt::Test_sqrt()
{
}

Test_sqrt::~Test_sqrt()
{
}

// Сравнение с эталонной функцией из библиотеки cmath
void Test_sqrt::test_sqrt_result_data()
{
    QTest::addColumn<double>("x");
    QTest::addColumn<double>("y");
    QTest::addColumn<double>("result");

    for(int i = 0; i < 10; ++i)
    {
        double x = rand() % 1000;
        double y = rand() % 1000;

        QTest::newRow("sqrt") << x << y << std::sqrt(x);
//        qDebug() << x;
//        qDebug() << y;
    }
}

void Test_sqrt::test_sqrt_result()
{
    Sqrt test_sqrt;
    QFETCH(double, x);
    QFETCH(double, y);
    QFETCH(double, result);
    QCOMPARE(test_sqrt.sqrt_result(x, y), result);
}

QTEST_APPLESS_MAIN(Test_sqrt)

#include "tst_test_sqrt.moc"
