QT += testlib
QT -= gui

CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

TEMPLATE = app

SOURCES +=  tst_test_sqrt.cpp \
    ../lab2_sqrt/sqrt.cpp

HEADERS += \
    ../lab2_sqrt/sqrt.h
