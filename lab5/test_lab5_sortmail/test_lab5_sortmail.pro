QT += testlib
QT -= gui

CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

TEMPLATE = app

SOURCES +=  tst_test_sortmail.cpp \
    ../lab5_sortmail/sort_mail.cpp

HEADERS += \
    ../lab5_sortmail/sort_mail.h

DESTDIR = $$PWD

DISTFILES += \
    ../lab5_sortmail/mail_lab_5.txt
