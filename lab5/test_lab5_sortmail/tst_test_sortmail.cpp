#include <QtTest>
#include "../lab5_sortmail/sort_mail.h"

class Test_sortmail : public QObject
{
    Q_OBJECT

public:
    Test_sortmail();
    ~Test_sortmail();

private slots:
    void test_check_file();

};

Test_sortmail::Test_sortmail()
{
}

Test_sortmail::~Test_sortmail()
{
}

// Тест на существование файла, так же проверка на открытие есть в коде проекта
void Test_sortmail::test_check_file()
{
    Sort_mail test_sortmail;
    QCOMPARE(test_sortmail.check_file(), 1);
}

QTEST_APPLESS_MAIN(Test_sortmail)

#include "tst_test_sortmail.moc"
