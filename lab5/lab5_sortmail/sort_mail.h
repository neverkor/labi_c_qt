#include <string>
#include <vector>
#include <map>
#include <QtWidgets/QApplication>
#include <QtWidgets/QMainWindow>
#include <QtCharts/QChartView>
#include <QtCharts/QBarSeries>
#include <QtCharts/QBarSet>
#include <QtCharts/QLegend>
#include <QtCharts/QBarCategoryAxis>
#include <QtCharts/QValueAxis>
#include <iostream>
#include <fstream>
#include <QTextCodec>
QT_CHARTS_USE_NAMESPACE
using namespace std;

class Sort_mail
{
public:
    string line;
    string line_result;
    vector<string> result_from;
    vector<string> result_spam;
    vector<float> result_spam_float;
    map<string, int> result;
    float count_spam = 0;
    int count = 1;

    int check_file();
    void add_result();
    void chech_spam();
};
