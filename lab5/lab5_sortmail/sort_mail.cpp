#include "sort_mail.h"
#include <QDebug>

int Sort_mail::check_file()
{
    ifstream fin("mail_lab_5.txt");
    if (fin)
    {
        while (getline(fin, line))
        {
            if (line.find("From:", 0) != string::npos)
            {
                size_t pos = line.find("From:", 0);
                line_result = line.substr(pos + 6);
                result_from.push_back(line_result);
            }
            if (line.find("X-DSPAM-Confidence:", 0) != string::npos)
            {
                size_t pos = line.find("X-DSPAM-Confidence:", 0);
                line_result = line.substr(pos + 20);
                result_spam.push_back(line_result);
            }
        }
        fin.close();
        return 0;
    }
    else
    {
        QTextCodec *codec = QTextCodec::codecForName("CP866");
        QByteArray encodedString = codec->fromUnicode("Файл не найден");
        cout << encodedString.data();
        return 1;
        exit(1);
    }
}

void Sort_mail::add_result()
{
    result.insert(pair <string, int> (result_from[0], 0));
    for (unsigned int i = 0; i < result_from.size(); i++)
    {
        result.insert(pair <string, int> (result_from[i], 0));
        for (auto it = result.begin(); it != result.end(); it++)
        {
            if (result_from[i] == it->first)
            {
                it->second += 1;
            }
        }
    }
}

void Sort_mail::chech_spam()
{
    for (vector<string>::iterator it = result_spam.begin(); it != result_spam.end(); it++)
    {
        float buffer;
        buffer = atof((*it).c_str());
        result_spam_float.push_back(buffer);
    }

    count_spam = accumulate(result_spam_float.begin(), result_spam_float.end(), 0.0);
    count_spam = count_spam / result_spam_float.size();
    // Костыль для отображения кириллицы в консоли
    QTextCodec *codec = QTextCodec::codecForName("CP866");
    QByteArray encodedString = codec->fromUnicode("Среднее значение параметра X-DSPAM-Confidence: ");
    cout << encodedString.data();
    cout << count_spam << endl << endl;
    encodedString = codec->fromUnicode("Кого забанить? ");
    cout << encodedString.data();
    for (unsigned int i = 0; i < result_spam_float.size(); i++)
    {
        if (result_spam_float[i] == 1)
        {
            cout << result_from[i] << "; ";
        }
    }
    cout << endl << endl;
    encodedString = codec->fromUnicode("Список ящиков: ");
    cout << encodedString.data();
    cout << endl;
}
