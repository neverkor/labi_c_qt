CONFIG += c++11 console
QT += charts

SOURCES += \
    main.cpp \
    sort_mail.cpp

INSTALLS += target

HEADERS += \
    sort_mail.h

DESTDIR = $$PWD

DISTFILES += \
    mail_lab_5.txt
