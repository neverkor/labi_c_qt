#include <sort_mail.h>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    Sort_mail sort_mail;
    sort_mail.check_file();
    sort_mail.add_result();
    sort_mail.chech_spam();

    QBarSeries *series = new QBarSeries();
    QString bar_set;
    for (auto it = sort_mail.result.begin(); it != sort_mail.result.end(); it++)
    {
        cout << sort_mail.count << ". " << it->first << endl;
        bar_set = QString::number(sort_mail.count);
        sort_mail.count += 1;
        QBarSet *set = new QBarSet(bar_set);
        *set << it->second;
        series->append(set);
    }

    QChart *chart = new QChart();
    chart->addSeries(series);
    chart->setTitle("Почта - кол-во писем");
    chart->setAnimationOptions(QChart::SeriesAnimations);

    QStringList categories;
    categories << "Почта";
    QBarCategoryAxis *axisX = new QBarCategoryAxis();
    axisX->append(categories);
    chart->addAxis(axisX, Qt::AlignBottom);
    series->attachAxis(axisX);

    QValueAxis *axisY = new QValueAxis();
    axisY->setRange(0,200);
    chart->addAxis(axisY, Qt::AlignLeft);
    series->attachAxis(axisY);

    chart->legend()->setVisible(true);
    chart->legend()->setAlignment(Qt::AlignBottom);


    QChartView *chartView = new QChartView(chart);
    chartView->setRenderHint(QPainter::Antialiasing);

    QMainWindow window;
    window.setCentralWidget(chartView);
    window.resize(1600, 700);
    window.show();

    return a.exec();
}
