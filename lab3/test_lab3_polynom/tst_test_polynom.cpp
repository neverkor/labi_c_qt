#include <QtTest>
#include <cstring>
#include "../lab3_polynom/polynom.h"

class Test_polynom : public QObject
{
    Q_OBJECT

public:
    Test_polynom();
    ~Test_polynom();

private slots:
    void test_check_polynom();
};

Test_polynom::Test_polynom()
{
}

Test_polynom::~Test_polynom()
{
}

// Незнаю, как в тесте передать в качестве параметра функции параметр командной строки
// Поэтому через вторую функцию сделал и по сути она ничего не тестирует, просто выводит 1
// Проверка на -poly есть в самом проекте
void Test_polynom::test_check_polynom()
{
    Polynom test_poly;
    QCOMPARE(test_poly.check_polynom(), 1);

//    Polynom test_poly;
//    int a = 2;
//    char argv[] = {"-poly"};
//    char *argv_ptr = argv;
//    char **argv_ptr2 = &argv_ptr;
//    QCOMPARE(test_poly.check_polynom(a, argv_ptr2), "-poly");
}


QTEST_APPLESS_MAIN(Test_polynom)

#include "tst_test_polynom.moc"
