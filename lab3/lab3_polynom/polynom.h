#ifndef POLYNOM_H
#define POLYNOM_H
#include <iostream>
#include <sstream>
#include <QTextCodec>
using namespace std;

class Polynom
{
public:
    Polynom();
    double counting(int argc, char** argv);
    int check_polynom();
private:
    double polynom, result, sum_result;
};

#endif // POLYNOM_H
