#include <QCoreApplication>
#include "polynom.h"
#include "QDebug"

int main(int argc, char *argv[])
{
    Polynom poly;
    if (argc <= 1)
    {
        QTextCodec *codec = QTextCodec::codecForName("CP866");
        QByteArray encodedString = codec->fromUnicode("Нет аргументов. Программа завершена.");
        cout << encodedString.data() << endl;
        return 0;
    }
    // Если первый аргумент -poly
    else if (!strcmp(argv[1], "-poly"))
    {
        poly.counting(argc, argv);
        poly.check_polynom();
    }
    else
    {
        QTextCodec *codec = QTextCodec::codecForName("CP866");
        QByteArray encodedString = codec->fromUnicode("Не верные аргументы.");
        cout << encodedString.data() << endl;
    }

    QCoreApplication a(argc, argv);
    return 0;
}
